//database setting
var mongoose  = require('mongoose');
mongoose.connect('mongodb://localhost/hello-world');
//all mongoose object start with schema
var Schema   = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var kittySchema = mongoose.Schema({
    name:String
});

var Kitten = mongoose.model('Kitten', kittySchema);

exports.Kitten = Kitten;
