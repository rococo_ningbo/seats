//import objects in models.js
var models = require('../models');
var Kitten = models.Kitten;

exports.index = function(req,res){
    var kitten = new Kitten({name:Math.random()}).save(function(err,kitten,count){
    if (!err){
        console.log(kitten.name+' saved');
    }
    else{
        console.log(err);
    }
    });
    res.render('hello',{title:'hello world demo'});
} 

exports.list = function(req, res){
    var kitten_list = Kitten.find(function(err,list,count){
        res.render('list',{list:list});
    });
}
