//import objects
var express  = require('./app');
var app = express.app;

var routes = require('./routes');
var hello = require('./routes/hello');

//set urls
app.get('/',routes.index)
app.get('/hello',hello.index);
app.get('/list',hello.list);
